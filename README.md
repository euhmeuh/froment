# froment

WASM game engine based on Grain

**Project cancelled, Grain is definitely not mature yet. Don't use this.**

## Dependencies

  - Parcel (js packer)
  - Wasmer (WASI implementation)
  - Grain (language)

## License

Copyright (c) 2021 Zoé Martin <zoe@rilouw.eu> (https://rilouw.eu)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

