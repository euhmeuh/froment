import { WASI } from '@wasmer/wasi'
import browserBindings from '@wasmer/wasi/lib/bindings/browser'
import { WasmFs } from '@wasmer/wasmfs'

const wasmFs = new WasmFs();
const wasmFilePath = "/index.wasm";

let wasi = new WASI({
  args: [wasmFilePath],
  env: {},
  bindings: {
    ...browserBindings,
    fs: wasmFs.fs
  }
});

const startWasiTask = async pathToWasmFile => {
  const response  = await fetch(pathToWasmFile)
  const wasmBytes = new Uint8Array(await response.arrayBuffer())
  const wasmMemory = new WebAssembly.Memory({initial:10, maximum:100, shared: true});
  const wasmModule = await WebAssembly.compile(wasmBytes);
  const wasmInstance = await WebAssembly.instantiate(wasmModule, {
     ...wasi.getImports(wasmModule),
     js: {
       videoMemory: wasmMemory
     }
  });

  wasi.start(wasmInstance);
  initVideo(wasmInstance.exports, wasmMemory, 220, 220);

  console.log(await wasmFs.getStdOut());
};

const initVideo = (wasmInterface, videoMemory, width, height) => {
  console.log(wasmInterface)
  wasmInterface.init(width, height);
  const canvas = document.getElementById('screen-canvas');
  const gl = canvas.getContext('webgl');
  const buffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
  gl.bufferData(gl.ARRAY_BUFFER, videoMemory.buffer, gl.STATIC_DRAW);
};

startWasiTask(wasmFilePath);
